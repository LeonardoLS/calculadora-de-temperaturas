/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label1;
    @FXML
    private Label respf;
    @FXML
    private Label respk;
    @FXML
    private Label respc;
    @FXML
    private Button celsius;
    @FXML
    private Button fah;
    @FXML
    private Button kelvin;
    @FXML
    private Button but1;
    @FXML
    private Button but2;
    @FXML
    private Button but3;
    @FXML
    private Button but4;
    @FXML
    private Button but5;
    @FXML
    private Button but6;
    @FXML
    private Button but7;
    @FXML
    private Button but8;
    @FXML
    private Button but9;
    @FXML
    private Button but0; 
    @FXML
    private Button sair;
    private String tela="";
    private String escala="";
    private double valor1;
    private double valor2;
    private double valor3;

    
    @FXML
    private void handleButtonAction(ActionEvent event) {
    }
    
    @FXML
    private void calcular() {
       
    }
    @FXML
    private void add1(){
        tela+="1";
        label1.setText(tela);
    }
    @FXML
    private void add2(){
        tela+="2";
        label1.setText(tela);
    }
    @FXML
    private void add3(){
        tela+="3";
        label1.setText(tela);
    }
    @FXML
    private void add4(){
        tela+="4";
        label1.setText(tela);
    }
    @FXML
    private void add5(){
        tela+="5";
        label1.setText(tela);
    }
    @FXML
    private void add6(){
        tela+="6";
        label1.setText(tela);
    }
    @FXML
    private void add7(){
        tela+="7";
        label1.setText(tela);
    }
    @FXML
    private void add8(){
        tela+="8";
        label1.setText(tela);
    }
    @FXML
    private void add9(){
        tela+="9";
        label1.setText(tela);
    }
    @FXML
    private void add0(){
        tela+="0";
        label1.setText(tela);
    }
    @FXML
    private void celsius(){
        escala="C";
        label1.setText(tela+" "+escala);
        valor1=Integer.parseInt(tela);
        respc.setText(tela+"C");
        valor2=(valor1*1.8)+32;
        respf.setText(valor2+"F");
        valor3=valor1+273;
        respk.setText(valor3+"K");
    }
    @FXML
    private void fahrenheit(){
        escala="F";
        label1.setText(tela+" "+escala);
        valor1=Integer.parseInt(tela);
        respf.setText(tela+"F");
        valor2=(valor1-32)/1.8;
        respc.setText(valor2+"C");
        valor3=valor2+273;
        respk.setText(valor3+"K");
    }
    @FXML
    private void kelvin(){
        escala="K";
        label1.setText(tela+" "+escala);
        valor1=Integer.parseInt(tela);
        respk.setText(tela+"K");
        valor2=valor1-273;
        respc.setText(valor2+"C");
        valor3=(valor2*1.8)+32;
        respf.setText(valor3+"F");
    }
   
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
